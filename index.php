<?php
	include('include/DBConn.php');
	$getMaterialTypeSQL = "SELECT * FROM material_step_1 GROUP BY MaterialName";
	$getMaterialChildrenSQL = "SELECT * FROM material_children GROUP BY MaterialChildrenName";
	$getComponentTypeSQL = "SELECT * FROM component_type GROUP BY ComponentName";
?>
<!DOCTYPE html>
<html>
<head>
	<title>First App</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<script type="text/javascript" src="javascript/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="javascript/process.js"></script>
	<script type="text/javascript">
		function validateData(){
			if(document.thinningdata.MaterialStep1.value == ''){
				$('#MaterialStep1_Input').html("*required");
				return false;
			} else {
				$('.notification').html("");
			}
			
			if(document.thinningdata.Material.value == ''){
				$('#Material_Input').html("*required");
				return false;
			} else {
				$('.notification').html("");
			}
			
			if(document.thinningdata.ComponentType.value == ''){
				$('#ComponentType_Input').html("*required");
				return false;
			} else {
				$('.notification').html("");
			}
			
			var checkOK = "0123456789 \t\r\n\f";
			var checkStr = document.thinningdata.RBIAnalysisYear.value;
			var allValid = true;
			for (i = 0;  i < checkStr.length;  i++)
			{
				ch = checkStr.charAt(i);
				for (j = 0;  j < checkOK.length;  j++)
					if (ch == checkOK.charAt(j))
						break;
					if (j == checkOK.length)
					{
						allValid = false;
						break;
					}
			}
			
			if (!allValid)
			{
				$('#RBIAnalysisYear').html('* YEAR type input');
				$("#RBIAnalysisYear").focus();
				return (false);
			} else {
				$('.notification').html("");
			}
			
			
			calculateData;
		}
	</script>
</head>
<body>
	<div id="main-bg">
		<div id="main-content">
			<div class="container">
				<div id="wrapper">
					<div style="border-bottom:1px dotted #A8A8A8">
					<h3>Tugas Besar Basis Data</h3>
					<p>Dibuat oleh Pushmov's Lab</p>
					</div>
					<div id="form-wrapper" style="padding:10px 30px;">
						<div class="first-col" id="first-col" align="center"><img src="images/icon.png"></div>
						
						<div class="second-col" id="json-result" style="display:none;">
							<div style="float:left">
							<div class="row-1">
								<div class="row-label result-label">Mat Value 1</div>: <span id="MatValue1"></span>
							</div>
							
							<div class="row-1">
								<div class="row-label result-label">Mat Value</div>: <span id="MatValue"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Mat Clad Value 1</div>: <span id="MatCladValue1"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Mat Clad Value</div>: <span id="MatCladValue"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label">Comp Type Code</div>: <span id="CompTypeCode"></span>
							</div>
							
							<div class="row-1">
								<div class="row-label result-label">CR BASE TOTAL</div>: <span id="CRBASETOTAL"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">CR CLAD TOTAL</div>: <span id="CRCLADTOTAL"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">THINNING TYPE</div>: <span id="ThinningType"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label">OLM</div>: <span id="OLM"></span>
							</div>
							
							<div class="row-1">
								<div class="row-label result-label">age</div>: <span id="Age"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">t min 653 Tank Bottom (in)</div>: <span id="tMin653TankBottom_in"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">t min 579 used (mm)</div>: <span id="tMin579Used_mm"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label">age rc</div>: <span id="Age_RC"></span>
							</div>
							
							<div class="row-1">
								<div class="row-label result-label">Art NON CLAD (CR module)</div>: <span id="ArtNonClad_CRModule"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art NON CLAD (input)</div>: <span id="ArtNonClad_input"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art CLAD (CR module)</div>: <span id="ArtClad_CRModule"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art CLAD (input CR clad only)</div>: <span id="ArtClad_inputCRCladOnly"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art CLAD (all CR input)</div>: <span id="ArtClad_allCRInput"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label"><strong>Art FIX</strong></div>: <span id="ArtFIX"></span>
							</div>
							</div>
							<div style="float:right;margin-right:20px">
							<div class="row-1">
								<div class="row-label result-label">Insp Kode Tank Bottom</div>: <span id="InspCodeTankBottom"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Insp Kode OTHERS</div>: <span id="InspCodeOTHERS"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art Tank Bottom Kode</div>: <span id="ArtTankBottomCode"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art OTHERS Kode</div>: <span id="ArtOTHERSCode"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label"><strong>DF base</strong></div>: <span id="DFbase"></span>
							</div>
							
							<div class="row-1">
								<div class="row-label result-label">Fip Piping</div>: <span id="FipPiping"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Fdl Piping</div>: <span id="FdlPiping"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Fwd Tank</div>: <span id="FwdTank"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Fam Tank</div>: <span id="FamTank"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Fsm Tank Bottom</div>: <span id="FsmTankBottom"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Fom</div>: <span id="Fom"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label"><strong>DF THINNING</strong></div>: <span id="DFTHINNING"></span>
							</div>
							
							<div class="row-1">
								<div class="row-label result-label">Screening</div>: <span id="Screening"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Art thinning</div>: <span id="ArtThinning"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Thinning Type</div>: <span id="ThinningType2"></span>
							</div>
							<div class="row-1">
								<div class="row-label result-label">Damage Factor for Thinning</div>: <span id="DamageFactor"></span>
							</div>
							<div class="row-1 result-footer">
								<div class="row-label result-label">Major Corrosion from Module</div>: <span id="MajorCorrosion"></span>
							</div>
							
							<div class="row-1 result-footer">
								<button onclick="backButton()">BACK</button>
							</div>
							</div>
						</div>
						
						<div class="second-col" id="result">
						<form name="thinningdata" id="thinning-form" action="calculate.php" method="post" onsubmit="return calculateData()">
						<div style="float:left">
							<div class="row-1">
								<div class="row-label"><label>Material  STEP 1 :</label></div>
								<select name="MaterialStep1" onblur="return validateData()">
									<option value="">Select material type</option>
									<?php 
										$MaterialType = mysql_query($getMaterialTypeSQL);
										while($MaterialTypeRow = mysql_fetch_array($MaterialType)) :
									?>
										<option value="<?php echo $MaterialTypeRow['MaterialID']?>"><?php echo $MaterialTypeRow['MaterialName']?></option>
									<?php endwhile;?>
								</select>
								<div class="notification" id="MaterialStep1_Input"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Material :</label></div>
								<select id="Material" name="Material" onblur="return validateData()">
									<option value="">Select material type</option>
									<?php 
										$MaterialType = mysql_query($getMaterialChildrenSQL);
										while($MaterialTypeRow = mysql_fetch_array($MaterialType)) :
									?>
										<option value="<?php echo $MaterialTypeRow['MaterialChildrenID']?>"><?php echo $MaterialTypeRow['MaterialChildrenName']?></option>
									<?php endwhile;?>
								</select>
								<div class="notification" id="Material_Input"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Material of Clad STEP 1 :</label></div>
								<select id="MaterialType" name="MaterialType">
									<option value="">Select material type</option>
									<option value="none">None</option>
									<?php 
										$MaterialType = mysql_query($getMaterialTypeSQL);
										while($MaterialTypeRow = mysql_fetch_array($MaterialType)) :
									?>
										<option value="<?php echo $MaterialTypeRow['MaterialID']?>"><?php echo $MaterialTypeRow['MaterialName']?></option>
									<?php endwhile;?>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Material of Clad :</label></div>
								<select id="CladMaterial" name="CladMaterial">
									<option value="">Select material type</option>
									<?php 
										$MaterialType = mysql_query($getMaterialChildrenSQL);
										while($MaterialTypeRow = mysql_fetch_array($MaterialType)) :
									?>
										<option value="<?php echo $MaterialTypeRow['MaterialChildrenID']?>"><?php echo $MaterialTypeRow['MaterialChildrenName']?></option>
									<?php endwhile;?>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Component Type :</label></div>
								<select id="ComponentType" name="ComponentType" onblur="return validateData()">
									<option value="">Select Component Type</option>
									<?php 
										$MaterialType = mysql_query($getComponentTypeSQL);
										while($MaterialTypeRow = mysql_fetch_array($MaterialType)) :
									?>
										<option value="<?php echo $MaterialTypeRow['ComponentID']?>"><?php echo $MaterialTypeRow['ComponentName']?></option>
									<?php endwhile;?>
								</select>
								<div class="notification" id="ComponentType_Input"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Date Now (RBI analysis year) :</label></div>
								<input type="text" id="RBIAnalysisYear" name="RBIAnalysisYear" maxlength="4" onblur="return validateData()">
								<div class="notification" id="RBIAnalysisYear_Input"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Component Start Year :</label></div>
								<input type="text" id="ComponentStartYear" name="ComponentStartYear" maxlength="4">
								<div class="notification">* Input year</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label>Last Thickness Reading Year :</label></div>
								<input type="text" id="LastThickentssReadingYear" name="LastThickentssReadingYear" maxlength="4">
								<div class="notification">* Input year</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label>Corrosion Rate obtained from :</label></div>
								<select name="CorrosionRateFrom" id="CorrosionRateFrom">
									<option value="MeasuredInput">Measured input</option>
									<option value="Module">Estimated from module</option>
									<option value="CR">CR clad input, CR base module</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label>Input Corrosion rate Clad Material (mpy) :</label></div>
								<input type="text" id="CorrosionRateClad" name="CorrosionRateClad" >
								<div class="notification">* Input integer</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label>Input Corrosion rate Base Metal (mpy) :</label></div>
								<input type="text" id="CorrosionRateBase" name="CorrosionRateBase">
								<div class="notification">* Input integer</div>
							</div>
						
							<div class="row-1">
								<div class="row-label"><label>Input Thinning Type :</label></div>
								<select name="ThinningType" id="ThinningType">
									<option value="Module">Estimated by Module</option>
									<option value="General">General</option>
									<option value="Local">Local</option>
								</select>
								<div class="notification"></div>
							</div>
							<div class="row-1">
								<div class="row-label"><label >Number of Inspection for Thinning :</label></div>
								<input type="text" id="InspectionNumber" name="InspectionNumber">
								<div class="notification">* Input integer</div>
							</div>
							
						</div>
						<div style="float:right;margin-right:20px;">
							<div class="row-1">
								<div class="row-label"><label >Inspection Effectiveness for Thinning :</label></div>
								<input type="text" id="InspectionEffectiveness" name="InspectionEffectiveness" maxlength="1">
								<!--Limited Input A,B,C,D,E-->
								<div class="notification">* Input alphabet A/B/C/D/E</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Thickness at Last Inspection (mm) :</label></div>
								<input type="text" id="LastThickness" name="LastThickness">
								<div class="notification">* Input integer</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Furnished thickness of base metal only (mm)	 :</label></div>
								<input type="text" id="FurnishedThickness" name="FurnishedThickness">
								<div class="notification">* Input integer</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >t min based on API 579 Annex A (mm) :</label></div>
								<input type="text" id="TMin" name="TMin" >
								<div class="notification">* Input integer</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Corrosion Allowance (mm) :</label></div>
								<input type="text" id="CorrosionAllowance" name="CorrosionAllowance">
								<div class="notification">* Input integer</div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Online Monitoring for Thinning :</label></div>
								<select id="ThinningMonitoring" name="ThinningMonitoring" >
									<option value="none">None</option>
									<option value="KPV">Key Process Variable</option>
									<option value="KPV-P">Key Process Variable conjunction with Probes</option>
									<option value="ERP">Electrical Resistance Probes</option>
									<option value="CC">Corrosion Coupons</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Injection Point (for Piping) :</label></div>
								<select id="InjectionPoint" name="InjectionPoint" >
									<option value="YWithAPI570">Yes and inspected as API 570</option>
									<option value="Y">Yes, but not inspected</option>
									<option value="N">No</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Dead Legs (for Piping) :</label></div>
								<select id="DeadLegs" name="DeadLegs" >
									<option value="YHI">Yes and Highly inspected</option>
									<option value="Y">Yes, but not inspected</option>
									<option value="N">No</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Welded (for Tank) :</label></div>
								<select id="Welded" name="Welded" >
									<option value="Y">Yes</option>
									<option value="N">No</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Maintenance with API 653 (for Tank) :</label></div>
								<select id="API653" name="API653" >
									<option value="Y">Yes</option>
									<option value="N">No</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Settlement (for Tank) :</label></div>
								<select id="Settlement" name="Settlement" >
									<option value="1">Recorded and exceeds API 653 criteria</option>
									<option value="2">Recorded and meets API 653 criteria</option>
									<option value="3">Never Evaluated</option>
									<option value="4">Concrete foundation, No Settlement</option>
								</select>
								<div class="notification"></div>
							</div>
							
							<div class="row-1">
								<div class="row-label"><label >Release Prevention Barier (for Tank Bottom) :</label></div>
								<select id="ReleasePreventionBarier" name="ReleasePreventionBarier" >
									<option value="RPBAPI650">RPB exists per API 650</option>
									<option value="RPB">RPB exists but Not per API 650</option>
									<option value="NoRPB">No RPB or Single Bottom</option>
								</select>
								<div class="notification"></div>
							</div>
							
							
							<div class="row-2">
								<input type="submit" class="form-reset" name="reset-data" value="RESET">
								<input type="submit" onclick="return validateData()" class="form-submit" name="submit-data" value="Calculate">
							</div>
						</div>
						</form>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="container" align="right" style="padding-right:30px;">
				<h4>Created by Pushmov's Lab</h4>
				<span>Copyright &copy; 2012</span>
			</div>
		</div>
	</div>
</body>
</html>