-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 04, 2012 at 04:23 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `t.thinning`
--

-- --------------------------------------------------------

--
-- Table structure for table `component_type`
--

CREATE TABLE IF NOT EXISTS `component_type` (
  `ComponentID` int(20) NOT NULL AUTO_INCREMENT,
  `ComponentName` varchar(255) DEFAULT NULL,
  `ComponentValue` int(5) DEFAULT NULL,
  PRIMARY KEY (`ComponentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `component_type`
--

INSERT INTO `component_type` (`ComponentID`, `ComponentName`, `ComponentValue`) VALUES
(1, 'PIPE-10', 11),
(2, 'COMPC', 1),
(3, 'COMPR', 2),
(4, 'HEXSS', 3),
(5, 'HEXTS', 4),
(6, 'HEXTUBE', 5),
(7, 'PIPE-1', 6),
(8, 'PIPE-2', 7),
(9, 'PIPE-4', 8),
(10, 'PIPE-6', 9),
(11, 'PIPE-8', 10),
(12, 'PIPE-12', 12),
(13, 'PIPE-16', 13),
(14, 'PIPEGT16', 14),
(15, 'PUMP1S', 15),
(16, 'PUMP2S', 16),
(17, 'PUMPR', 17),
(18, 'TANKBOTTOM', 18),
(19, 'COURSE-1', 19),
(20, 'COURSE-2', 20),
(21, 'COURSE-3', 21),
(22, 'COURSE-4', 22),
(23, 'COURSE-5', 23),
(24, 'COURSE-6', 24),
(25, 'COURSE-7', 25),
(26, 'COURSE-8', 26),
(27, 'COURSE-9', 27),
(28, 'COURSE-10', 28),
(29, 'COLTOP', 29),
(30, 'COLMID', 30),
(31, 'COLBTM', 31),
(32, 'DRUM', 32),
(33, 'KODRUM', 33),
(34, 'FINFAN', 34),
(35, 'FILTER', 35),
(36, 'REACTOR', 36),
(37, 'HEATER', 37);

-- --------------------------------------------------------

--
-- Table structure for table `corrosion`
--

CREATE TABLE IF NOT EXISTS `corrosion` (
  `CorrosionID` int(5) NOT NULL AUTO_INCREMENT,
  `MaterialCorrosionName` varchar(255) DEFAULT NULL,
  `Screening` varchar(20) DEFAULT NULL,
  `CRBase` varchar(20) DEFAULT NULL,
  `CRClad` int(5) DEFAULT NULL,
  `ThinningType` varchar(20) DEFAULT NULL,
  `OLM` int(5) DEFAULT NULL,
  PRIMARY KEY (`CorrosionID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `corrosion`
--

INSERT INTO `corrosion` (`CorrosionID`, `MaterialCorrosionName`, `Screening`, `CRBase`, `CRClad`, `ThinningType`, `OLM`) VALUES
(1, 'HCL', 'Yes', '7', 0, 'Local', 20),
(2, 'HTSNA', 'No', '0', 0, NULL, 1),
(3, 'HTH2SH2', 'No', '0', 0, NULL, 1),
(4, 'H2SO4', 'No', '0', 0, NULL, 1),
(5, 'HF', 'No', '0', 0, NULL, 1),
(6, 'SW', 'Yes', '15', 0, 'General', 20),
(7, 'Amine', 'No', '0', 0, NULL, 1),
(8, 'HTO', 'False', '0', 0, 'General', 1),
(9, 'ASW', 'No', '0', 0, NULL, 1),
(10, 'CW', 'No', '0', 0, NULL, 1),
(11, 'SOIL', 'No', '0', 0, NULL, 1),
(12, 'CO2', 'Yes', '38.6179', 0, 'Local', 1),
(13, 'ASTB', 'No', '0', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `insp_code`
--

CREATE TABLE IF NOT EXISTS `insp_code` (
  `CodeName` varchar(4) NOT NULL DEFAULT '',
  `CodeValue` int(4) DEFAULT NULL,
  PRIMARY KEY (`CodeName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insp_code`
--

INSERT INTO `insp_code` (`CodeName`, `CodeValue`) VALUES
('1A', 6),
('1B', 5),
('1C', 4),
('1D', 3),
('2A', 10),
('2B', 9),
('2C', 8),
('2D', 7),
('3A', 14),
('3B', 13),
('3C', 12),
('3D', 11),
('4A', 18),
('4B', 17),
('4C', 16),
('4D', 15),
('5A', 22),
('5B', 21),
('5C', 20),
('5D', 19),
('6A', 26),
('6B', 25),
('6C', 24),
('6D', 23),
('E', 2);

-- --------------------------------------------------------

--
-- Table structure for table `material_children`
--

CREATE TABLE IF NOT EXISTS `material_children` (
  `MaterialChildrenID` int(20) NOT NULL AUTO_INCREMENT,
  `MaterialChildrenName` varchar(255) DEFAULT NULL,
  `MaterialChildrenType` enum('Base','Clad','Both') DEFAULT NULL,
  `MaterialChildrenValue` int(5) DEFAULT NULL,
  PRIMARY KEY (`MaterialChildrenID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `material_children`
--

INSERT INTO `material_children` (`MaterialChildrenID`, `MaterialChildrenName`, `MaterialChildrenType`, `MaterialChildrenValue`) VALUES
(1, 'Carbon Steel', 'Both', 1),
(2, 'C-0.5Mo (annealed)', 'Both', 2),
(3, 'C-0.5Mo (normalized)', 'Both', 3),
(4, '1Cr-0.2Mo', 'Both', 4),
(5, '1Cr-0.5Mo', 'Both', 5),
(6, '1.25Cr', 'Both', 6),
(7, '1.25Cr-0.5Mo', 'Both', 7),
(8, '2.25Cr', 'Both', 8),
(9, '2.25Cr-0.5Mo', 'Both', 9),
(10, '2.25Cr-1Mo', 'Both', 10),
(11, '3Cr-1Mo', 'Both', 11),
(12, '5Cr', 'Both', 12),
(13, '5Cr-0.5Mo', 'Both', 13),
(14, '5Cr-0.5Mo-Si', 'Both', 14),
(15, '7Cr', 'Both', 15),
(16, '7Cr-0.5Mo', 'Both', 16),
(17, '7Cr-1Mo', 'Both', 17),
(18, '9Cr', 'Both', 18),
(19, '9Cr-1Mo', 'Both', 19),
(20, '12Cr', 'Both', 20),
(21, '<13%Cr', 'Both', 21),
(22, '304 SS', 'Both', 22),
(23, '304 L SS', 'Both', 23),
(24, '304 H SS', 'Both', 24),
(25, '309 SS', 'Both', 25),
(26, '310 SS', 'Both', 26),
(27, '316 SS', 'Both', 27),
(28, '316 L SS', 'Both', 28),
(29, '316 H SS', 'Both', 29),
(30, '317 SS', 'Both', 30),
(31, '321 SS', 'Both', NULL),
(32, '321 H SS', 'Both', NULL),
(33, '347 SS', 'Both', NULL),
(34, '347 H SS', 'Both', NULL),
(35, '405 SS', 'Both', NULL),
(36, '430 SS', 'Both', NULL),
(37, '430 F SS', 'Both', NULL),
(38, '442 SS', 'Both', NULL),
(39, '446 SS', 'Both', NULL),
(40, 'Alloy 20 (Nicrofer)', 'Both', NULL),
(41, 'Alloy 400 (Monel)', 'Both', NULL),
(42, 'Alloys 600 (Inconel)', 'Both', NULL),
(43, 'Alloy 625 (Inconel)', 'Both', NULL),
(44, 'Alloys 800 (Incoloy)', 'Both', NULL),
(45, 'Alloys 800 H (Incoloy)', 'Both', NULL),
(46, 'Alloy 825 (Incoloy)', 'Both', NULL),
(47, 'Alloy B-2 (Hastelloy)', 'Both', NULL),
(48, 'Alloy C-276 (Hastelloy)', 'Both', NULL),
(49, 'HK Alloy', 'Both', NULL),
(50, 'HP Alloy', 'Both', NULL),
(51, 'Carbon Steel', 'Both', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `material_step_1`
--

CREATE TABLE IF NOT EXISTS `material_step_1` (
  `MaterialID` int(20) NOT NULL AUTO_INCREMENT,
  `MaterialName` varchar(255) DEFAULT NULL,
  `MaterialStep1Type` enum('Base','Clad','Both') DEFAULT NULL,
  `MaterialValue` int(5) DEFAULT NULL,
  PRIMARY KEY (`MaterialID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `material_step_1`
--

INSERT INTO `material_step_1` (`MaterialID`, `MaterialName`, `MaterialStep1Type`, `MaterialValue`) VALUES
(1, 'Carbon and Low Alloy Steel', 'Both', 1),
(2, 'Austenitic Stainless Steel', 'Both', 2),
(3, 'Ferritic Stainless Steel', 'Both', 3),
(4, 'Nickel Based Alloy', 'Both', 4);

-- --------------------------------------------------------

--
-- Table structure for table `others_code`
--

CREATE TABLE IF NOT EXISTS `others_code` (
  `Art` varchar(4) NOT NULL,
  `E` int(4) DEFAULT NULL,
  `1D` int(4) DEFAULT NULL,
  `1C` int(4) DEFAULT NULL,
  `1B` int(4) DEFAULT NULL,
  `1A` int(4) DEFAULT NULL,
  `2D` int(4) DEFAULT NULL,
  `2C` int(4) DEFAULT NULL,
  `2B` int(4) DEFAULT NULL,
  `2A` int(4) DEFAULT NULL,
  `3D` int(4) DEFAULT NULL,
  `3C` int(4) DEFAULT NULL,
  `3B` int(4) DEFAULT NULL,
  `3A` int(4) DEFAULT NULL,
  `4D` int(4) DEFAULT NULL,
  `4C` int(4) DEFAULT NULL,
  `4B` int(4) DEFAULT NULL,
  `4A` int(4) DEFAULT NULL,
  `5D` int(4) DEFAULT NULL,
  `5C` int(4) DEFAULT NULL,
  `5B` int(4) DEFAULT NULL,
  `5A` int(4) DEFAULT NULL,
  `6D` int(4) DEFAULT NULL,
  `6C` int(4) DEFAULT NULL,
  `6B` int(4) DEFAULT NULL,
  `6A` int(4) DEFAULT NULL,
  PRIMARY KEY (`Art`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `others_code`
--

INSERT INTO `others_code` (`Art`, `E`, `1D`, `1C`, `1B`, `1A`, `2D`, `2C`, `2B`, `2A`, `3D`, `3C`, `3B`, `3A`, `4D`, `4C`, `4B`, `4A`, `5D`, `5C`, `5B`, `5A`, `6D`, `6C`, `6B`, `6A`) VALUES
('0.02', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('0.04', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('0.06', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('0.08', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('0.10', 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('0.12', 6, 5, 3, 2, 1, 4, 2, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1),
('0.14', 20, 17, 10, 6, 1, 13, 6, 1, 1, 10, 3, 1, 1, 7, 2, 1, 1, 5, 1, 1, 1, 4, 1, 1, 1),
('0.16', 90, 70, 50, 20, 3, 50, 20, 4, 1, 40, 10, 1, 1, 30, 5, 1, 1, 20, 2, 1, 1, 14, 1, 1, 1),
('0.18', 250, 200, 130, 70, 7, 170, 70, 10, 1, 130, 35, 3, 1, 100, 15, 1, 1, 70, 7, 1, 1, 50, 3, 1, 1),
('0.20', 400, 300, 210, 110, 15, 290, 120, 20, 1, 260, 60, 5, 1, 180, 20, 2, 1, 120, 10, 1, 1, 100, 6, 1, 1),
('0.25', 520, 450, 290, 150, 20, 350, 170, 30, 2, 240, 80, 6, 1, 200, 30, 2, 1, 150, 15, 2, 1, 120, 7, 1, 1),
('0.30', 650, 550, 400, 200, 30, 400, 200, 40, 4, 320, 110, 9, 2, 240, 50, 4, 2, 180, 25, 3, 2, 150, 10, 2, 2),
('0.35', 750, 650, 550, 300, 80, 600, 300, 80, 10, 540, 150, 20, 5, 440, 90, 10, 4, 350, 70, 6, 4, 280, 40, 5, 4),
('0.40', 900, 800, 700, 400, 130, 700, 400, 120, 30, 600, 200, 50, 10, 500, 140, 20, 8, 400, 110, 10, 8, 350, 90, 9, 8),
('0.45', 1050, 900, 810, 500, 200, 800, 500, 160, 40, 700, 270, 60, 20, 600, 200, 30, 15, 500, 160, 20, 15, 400, 130, 20, 15),
('0.50', 1200, 1100, 970, 600, 270, 1000, 600, 200, 60, 900, 360, 80, 40, 800, 270, 50, 40, 700, 210, 40, 40, 600, 180, 40, 40),
('0.55', 1350, 1200, 1130, 700, 350, 1100, 750, 300, 100, 1000, 500, 130, 90, 900, 350, 100, 90, 800, 260, 90, 90, 700, 240, 90, 90),
('0.60', 1500, 1400, 1250, 850, 500, 1300, 900, 400, 230, 1200, 620, 250, 210, 1000, 450, 220, 210, 900, 360, 210, 210, 800, 300, 210, 210),
('0.65', 1900, 1700, 1400, 1000, 700, 1600, 1105, 670, 530, 1300, 880, 550, 500, 1200, 700, 530, 500, 1100, 640, 500, 500, 1000, 600, 500, 500);

-- --------------------------------------------------------

--
-- Table structure for table `tank_bottom_code`
--

CREATE TABLE IF NOT EXISTS `tank_bottom_code` (
  `Art` varchar(4) NOT NULL DEFAULT '',
  `E` int(4) DEFAULT NULL,
  `D` int(4) DEFAULT NULL,
  `C` int(4) DEFAULT NULL,
  `B` int(4) DEFAULT NULL,
  `A` int(4) DEFAULT NULL,
  PRIMARY KEY (`Art`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tank_bottom_code`
--

INSERT INTO `tank_bottom_code` (`Art`, `E`, `D`, `C`, `B`, `A`) VALUES
('0.05', 4, 1, 1, 1, 1),
('0.10', 14, 3, 1, 1, 1),
('0.15', 32, 8, 2, 1, 1),
('0.20', 56, 18, 6, 2, 1),
('0.25', 87, 32, 11, 4, 3),
('0.30', 125, 53, 21, 9, 6),
('0.35', 170, 80, 36, 16, 12),
('0.40', 222, 115, 57, 29, 21),
('0.45', 281, 158, 86, 47, 36),
('0.50', 347, 211, 124, 73, 58),
('0.55', 420, 273, 173, 109, 89),
('0.60', 500, 346, 234, 158, 133),
('0.65', 587, 430, 309, 222, 192),
('0.70', 681, 527, 401, 305, 270),
('0.75', 782, 635, 510, 409, 370),
('0.80', 890, 757, 638, 538, 498),
('0.85', 1005, 893, 789, 696, 658),
('0.90', 1126, 1044, 963, 888, 856),
('0.95', 1255, 1209, 1163, 1118, 1098),
('1.00', 1390, 1390, 1390, 1390, 1390);
