<?php
	/////Last modified 12/04/2012 by pushmov
	
	include('include/DBConn.php');
	
	//get the value of form
	$MaterialStep1 							= $_POST['MaterialStep1'];
	$Material 									= $_POST['Material'];
	$MaterialType 							= $_POST['MaterialType'];
	$CladMaterial 							= $_POST['CladMaterial'];
	
	$ComponentType 							= $_POST['ComponentType'];
	$RBIAnalysisYear 						= $_POST['RBIAnalysisYear'];
	$ComponentStartYear 				= $_POST['ComponentStartYear'];
	$LastThickentssReadingYear 	= $_POST['LastThickentssReadingYear'];
	
	$CorrosionRateFrom 					= $_POST['CorrosionRateFrom'];
	$CorrosionRateClad 					= $_POST['CorrosionRateClad'];
	$CorrosionRateBase 					= $_POST['CorrosionRateBase'];
	$InputThinningType 							= $_POST['ThinningType'];
	
	$InspectionNumber 					= $_POST['InspectionNumber'];
	$InspectionEffectiveness 		= $_POST['InspectionEffectiveness'];
	$LastThickness 							= $_POST['LastThickness'];
	$FurnishedThickness 				= $_POST['FurnishedThickness'];
	
	$TMin 											= $_POST['TMin'];
	$CorrosionAllowance 				= $_POST['CorrosionAllowance'];
	$ThinningMonitoring 				= $_POST['ThinningMonitoring'];
	$InjectionPoint 						= $_POST['InjectionPoint'];
	
	$DeadLegs 									= $_POST['DeadLegs'];
	$Welded 										= $_POST['Welded'];
	$API653 										= $_POST['API653'];
	$Settlement 								= $_POST['Settlement'];
	$ReleasePreventionBarier 		= $_POST['ReleasePreventionBarier'];
	
	//mat value 1 calculation
	$MaterialStep1Value = mysql_fetch_array(mysql_query("SELECT * FROM material_step_1 WHERE MaterialID = '$MaterialStep1'"));
	if ($MaterialStep1Value['MaterialValue'] != '') 
		$MatValue1 =  $MaterialStep1Value['MaterialValue'];
	else $MatValue1 = 0;
	
	//mat valua calculation
	$Resources2 = mysql_fetch_array(mysql_query("SELECT * FROM material_children WHERE MaterialChildrenID = '$Material'"));
	if ($Resources2['MaterialChildrenValue'] != '') 
		$MatValue =  $Resources2['MaterialChildrenValue'];
	else $MatValue = 0;
	
	//mat clad value 1 calculation
	$Resources3 = mysql_fetch_array(mysql_query("SELECT * FROM material_step_1 WHERE MaterialID = '$MaterialStep1'"));
	if ($Resources3['MaterialValue'] != '') 
		$MatCladValue1 =  $Resources3['MaterialValue'];
	else $MatCladValue1 = 0;
	
	$Resources4 = mysql_fetch_array(mysql_query("SELECT * FROM material_children WHERE MaterialChildrenID = '$CladMaterial'"));
	if ($Resources4['MaterialChildrenValue'] != '') 
		$MatCladValue =  $Resources4['MaterialChildrenValue'];
	else $MatCladValue = 0;
	
	$CompTypeResources = mysql_fetch_array(mysql_query("SELECT * FROM component_type WHERE ComponentID = '$ComponentType'"));
	if ($CompTypeResources['ComponentValue'] != ''){
		$CompTypeCode = $CompTypeResources['ComponentValue'];
	}else $CompTypeCode = 0;
	
	//get the Max CRBase first from corrosion Table
	$MaxCRBase 	= mysql_fetch_array(mysql_query("SELECT MAX(CRBase) AS maximum FROM corrosion"));
	$MaxValue 	= $MaxCRBase['maximum'];
	
	$CRBaseTotalMpy = $MaxValue;
	$CRBaseTotalMmy = $MaxValue * 0.0254;
	
	//get the all row information based on max CRBAse Above
	$CorrosionRow 	= mysql_fetch_array(mysql_query("SELECT * FROM corrosion WHERE CRBase = '$MaxValue'"));
	$CRClad 				= $CorrosionRow['CRClad'];
	$ThinningType 	= $CorrosionRow['ThinningType'];
	$OLM 						= mysql_fetch_array(mysql_query("SELECT MAX(OLM) AS OLMMax FROM corrosion"));
	
	$CRCladTotalMpy 	= $CRClad;
	$CRCladTotalMmy 	= $CRClad * 0.0254;
	
	$ThinningTypeMpy	= $ThinningType;
	$OLMMpy						= $OLM['OLMMax'];
	
	// <!-- START age calculation ///
	if($LastThickentssReadingYear == '0' || $LastThickentssReadingYear == ''){
		$Age  = $RBIAnalysisYear - $ComponentStartYear;
	} else $Age = $RBIAnalysisYear - $LastThickentssReadingYear;
	//END Age Calculation -->
	
	//<!-- START t min 653 tank bottom calc _in & _mm
	//init var :
	$tMinBool1 = false;
	$tMinBool2 = false;
	
	if ($ReleasePreventionBarier == 'RPBAPI650' || $ReleasePreventionBarier == 'RPB') $tMinBool1 = true;
	if ($CompTypeCode == '18') $tMinBool2 = true;
	
	if($tMinBool1 && $tMinBool2) {
		$tMin653TankBottom_in	= 0.005;
	} else {
		if ($ReleasePreventionBarier == 'NoRPB' && $CompTypeCode == '18') $tMin653TankBottom_in	= 0.1;
			else $tMin653TankBottom_in	= 0;
	}
	//END t min 653 tank bottom calc -->
	
	//Start calc t min 579
	if ($CompTypeCode == '18')
		$tMin579Used_mm = $tMin653TankBottom_in * 25.4;
	else 
		$tMin579Used_mm = $TMin;
	//End of calc t min 579 -->
	
	//Age RC calc :
	if ($MaterialType == 'none') {
		$Age_rc = 0;
	} else {
		if ($CorrosionRateFrom == 'Module'){
			$MaxParam1 = 0;
			$MaxParam2 = ($LastThickness - $FurnishedThickness) / $CRCladTotalMmy;
			$Age_rc = max(array($MaxParam1, $MaxParam2));
		} else {
			$MaxParam1 = 0;
			$MaxParam2 = ($LastThickness - $FurnishedThickness) / ($CorrosionRateBase * 0.0254);
			$Age_rc = max(array($MaxParam1, $MaxParam2));
		}
	}
	//End Age RC
	
	#--> start art non clad crmodule calc
	$maxParam1 = 0;
	$maxParam2 = (1 - ($LastThickness - $CRBaseTotalMmy * $Age) / ($tMin579Used_mm + $CorrosionAllowance));
	$ArtNonClad_CRModule = max(array($maxParam1, $maxParam2));
	#--> END
	
	#--> start art non clad input
	$MaxParam3_1 = 0;
	$MaxParam3_2 = (1 - (($LastThickness - $CorrosionRateBase * 0.0254 * $Age) / ($tMin579Used_mm + $CorrosionAllowance)));
	$ArtNonClad_input = max(array($MaxParam3_1, $MaxParam3_2));
	#--> END
	
	# <-- start art clad cr module calc
	$MaxParam4_1 = 0;
	$MaxParam4_2 = 1 - ((($LastThickness - ($CRCladTotalMmy * $Age_rc) - $CRBaseTotalMmy * ($Age - $Age_rc))) / ($tMin579Used_mm + $CorrosionAllowance));
	$ArtClad_CRModule = max(array($MaxParam4_1, $MaxParam4_2));
	# end of art clad cr module calc--> 
	
	# <!-- start art clad input cr clad only calculation
	$MaxParam5_1 = 0;
	$MaxParam5_2 = 1 - (($LastThickness - ($CorrosionRateClad*0.0254*$Age_rc) - $CRBaseTotalMmy*($Age - $Age_rc))/($tMin579Used_mm + $CorrosionAllowance));
	$ArtClad_inputCRCladOnly = max(array($MaxParam5_1, $MaxParam5_2));
	# end of art clad input cr clad only-->
	
	# <!-- start art clad all cr input
	$MaxParam6_1 = 0;
	$MaxParam6_2 = 1 - (($LastThickness - ($CorrosionRateClad*0.0254*$Age_rc) - $CRBaseTotalMmy*0.0254*($Age - $Age_rc))/($tMin579Used_mm + $CorrosionAllowance)) ;
	$ArtClad_allCRInput = max(array($MaxParam6_1, $MaxParam6_2));
	# end of all cr input -->
	
	# <!-- ART FIX 
	if($CorrosionRateFrom == 'MeasuredInput' && $MaterialType == 'none'){
		$ArtFix = $ArtNonClad_input;
	} elseif ($MaterialType == 'none' && ($CorrosionRateFrom == 'Module' || $CorrosionRateFrom == 'CR')) {
		$ArtFix = $ArtNonClad_CRModule;
	} elseif ($MaterialType != 'none' && $CorrosionRateFrom == 'MeasuredInput'){
		$ArtFix = $ArtClad_allCRInput;
	} elseif ($MaterialType != 'none' && $CorrosionRateFrom == 'Module'){
		$ArtFix = $ArtClad_CRModule;
	} elseif ($MaterialType != 'none' && $CorrosionRateFrom == 'CR'){
		$ArtFix = $ArtClad_inputCRCladOnly;
	}
	# end ART FIX-->
	
	//Insp code tank bottom
	switch($InspectionEffectiveness){
		case 'A' :
			$InspKodeTankBottom = 6;
			break;
		case 'B':
			$InspKodeTankBottom = 5;
			break;
		case 'C':
			$InspKodeTankBottom = 4;
			break;
		case 'D':
			$InspKodeTankBottom = 3;
			break;
		case 'E':
			$InspKodeTankBottom = 2;
			break;
	}
	
	//Insp code others 1 & 2 calc :
	if($InspectionEffectiveness == 'E'){
		$InspKodeOthers1 = 'E';
	} elseif($InspectionNumber > 6) {
		$InspKodeOthers1 = '6'.$InspectionEffectiveness;
	} else {
		$InspKodeOthers1 = $InspectionNumber.$InspectionEffectiveness;
	}
	
	$ValueInspCode = mysql_fetch_array(mysql_query("SELECT * FROM insp_code WHERE CodeName = '$InspKodeOthers1'"));
	// echo "SELECT * FROM insp_code WHERE CodeName = '$InspKodeOthers1'";
	// exit();
	$InspKodeOthers2 = $ValueInspCode['CodeValue'];
	
	$InspKodeOthers = $InspKodeOthers2.'/'.$InspKodeOthers1;
	//end insp code calc
	
	//art tank bottom code calc :
	if($ArtFix < 0.05){
		$ArtTankBottomCode = 0.05;
	} else $ArtTankBottomCode = $ArtFix;
	//end art tank
	
	if($ArtFix < 0.02){
		$ArtOthersCode = 0.02;
	} else $ArtOthersCode = $ArtFix;
	
	if($CompTypeCode == 18){
		$LookupValue = $ArtTankBottomCode;
		$IndexColumn = $InspectionEffectiveness;
		$getRowValue = mysql_fetch_array(mysql_query("SELECT * FROM `tank_bottom_code` WHERE Art <= $LookupValue ORDER BY Art DESC LIMIT 0,1"));
		$DFBase = $getRowValue[$IndexColumn];
	} else {
		$LookupValue = $ArtOthersCode;
		$getRowValue = mysql_fetch_array(mysql_query("SELECT * FROM `others_code` WHERE Art <= $LookupValue ORDER BY Art DESC LIMIT 0,1"));
		$DFBase = $getRowValue[$InspKodeOthers1];
	}
	
	if(($CompTypeCode >= 6) && ($CompTypeCode <= 14) && ($InjectionPoint == 'Y'))
		$FipPiping = 3;
	else $FipPiping = 1;
	
	if(($CompTypeCode >= 6) && ($CompTypeCode <= 14) && ($DeadLegs == 'Y'))
		$FdlPiping = 3;
	else $FdlPiping = 1;
	
	if(($CompTypeCode >= 18) && ($CompTypeCode <= 28) && ($Welded == 'N'))
		$FwdTank = 10;
	else $FwdTank = 1;
	
	if(($CompTypeCode >= 18) && ($CompTypeCode <= 28) && ($API653 == 'N'))
		$FamTank = 5;
	else $FamTank = 1;
	
	if($CompTypeCode == 18){
		switch($Settlement){
			case 1:
				$FsmTankBottom = 2;
				break;
			case 2:
				$FsmTankBottom = 1;
				break;
			case 3:
				$FsmTankBottom = 1.5;
				break;
			case 4:
				$FsmTankBottom = 1;
				break;
		}
	} else $FsmTankBottom = 1;
	
	if($DFBase == 1) $Fom = 1;
		else $Fom = $OLMMpy;
	
	$DFThinning = $DFBase*$FipPiping*$FdlPiping*$FwdTank*$FamTank*$FsmTankBottom/$Fom;
	
	$Screening = $CorrosionRow['Screening'];
	$ArtThinning = $ArtFix;
	if($CompTypeCode == 18) {
		$ThinningType = 'Local';
	} elseif($InputThinningType == 'Module'){
		$ThinningType = $ThinningTypeMpy;
	} else $ThinningType = $InputThinningType;
	
	$DamageFactor = $DFThinning;
	if($CorrosionRow['MaterialCorrosionName'] == '') $MajorCorrosion = 'None';
		elseif($CRBaseTotalMpy > $CRCladTotalMpy) $MajorCorrosion = $CorrosionRow['MaterialCorrosionName'];
			else $MajorCorrosion = '';
	
	$ResultArray = array(
		'MatValue1' => $MatValue1,
		'MatValue' => $MatValue,
		'MatCladValue1' => $MatCladValue1,
		'MatCladValue' => $MatCladValue,
		'CompTypeCode' => $CompTypeCode,
		'CRBaseTotal_mpy' => $CRBaseTotalMpy,
		'CRBaseTotal_mmy' => $CRBaseTotalMmy,
		'CRCladTotal_mpy' => $CRCladTotalMpy,
		'CRCladTotal_mmy' => $CRCladTotalMmy,
		'ThinningType' => $ThinningTypeMpy,
		'OLM' => $OLMMpy,
		'Age' => $Age,
		'tMin653TankBottom_in' => $tMin653TankBottom_in,
		'tMin579Used_mm' => $tMin579Used_mm,
		'Age_RC' => $Age_rc,
		'ArtNonClad_CRModule' => $ArtNonClad_CRModule,
		'ArtNonClad_input' => $ArtNonClad_input,
		'ArtClad_CRModule' => $ArtClad_CRModule,
		'ArtClad_inputCRCladOnly' => $ArtClad_inputCRCladOnly,
		'ArtClad_allCRInput' => $ArtClad_allCRInput,
		'ArtFIX' => $ArtFix,
		'InspCodeTankBottom' => $InspKodeTankBottom,
		'InspCodeOTHERS' => $InspKodeOthers,
		'ArtTankBottomCode' => $ArtTankBottomCode,
		'ArtOTHERSCode' => $ArtOthersCode,
		'DFbase' => $DFBase,
		'FipPiping' => $FipPiping,
		'FdlPiping' => $FdlPiping,
		'FwdTank' => $FwdTank,
		'FamTank' => $FamTank,
		'FsmTankBottom' => $FsmTankBottom,
		'Fom' => $Fom,
		'DFTHINNING' => $DFThinning,
		'Screening' => $Screening,
		'ArtThinning' => $ArtThinning,
		'ThinningType2' => $ThinningType,
		'DamageFactor' => $DamageFactor,
		'MajorCorrosion' => $MajorCorrosion
	);
	echo json_encode($ResultArray);
?>