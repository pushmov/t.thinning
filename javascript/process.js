
function calculateData(){
		var form = $('#thinning-form');
		$.ajax({
				type: "POST",
				url: form.attr('action'),
				data: form.serialize(),
				success: function(response){
					var json = response,
					obj = JSON.parse(response);
					$('#first-col').hide();
					$('#first-col').show('slow');
					$('#result').hide();
					$('#json-result').show('slow');
					
					// alert(obj.InspCodeTankBottom);
					$('#MatValue1').html(obj.MatValue1);
					$('#MatValue').html(obj.MatValue);
					$('#MatCladValue1').html(obj.MatCladValue1);
					$('#MatCladValue').html(obj.MatCladValue);
					$('#CompTypeCode').html(obj.CompTypeCode);
					$('#CRBASETOTAL').html(obj.CRBaseTotal_mpy+'(mpy) '+obj.CRBaseTotal_mmy+'(mmy)');
					$('#CRCLADTOTAL').html(obj.CRCladTotal_mpy+'(mpy) '+obj.CRCladTotal_mmy+'(mmy)');
					$('#ThinningType').html(obj.ThinningType);
					$('#OLM').html(obj.OLM);
					$('#Age').html(obj.Age);
					$('#tMin653TankBottom_in').html(obj.tMin653TankBottom_in);
					$('#tMin579Used_mm').html(obj.tMin579Used_mm);
					$('#Age_RC').html(obj.Age_RC);
					$('#ArtNonClad_CRModule').html(obj.ArtNonClad_CRModule);
					$('#ArtNonClad_input').html(obj.ArtNonClad_input);
					$('#ArtClad_CRModule').html(obj.ArtClad_CRModule);
					$('#ArtClad_inputCRCladOnly').html(obj.ArtClad_inputCRCladOnly);
					$('#ArtClad_allCRInput').html(obj.ArtClad_allCRInput);
					$('#ArtFIX').html(obj.ArtFIX);
					$('#InspCodeTankBottom').html(obj.InspCodeTankBottom);
					$('#InspCodeOTHERS').html(obj.InspCodeOTHERS);
					$('#ArtTankBottomCode').html(obj.ArtTankBottomCode);
					$('#ArtOTHERSCode').html(obj.ArtOTHERSCode);
					$('#DFbase').html(obj.DFbase);
					$('#FipPiping').html(obj.FipPiping);
					$('#FdlPiping').html(obj.FdlPiping);
					$('#FwdTank').html(obj.FwdTank);
					$('#FamTank').html(obj.FamTank);
					$('#FsmTankBottom').html(obj.FsmTankBottom);
					$('#Fom').html(obj.Fom);
					$('#DFTHINNING').html(obj.DFTHINNING);
					$('#Screening').html(obj.Screening);
					$('#ArtThinning').html(obj.ArtThinning);
					$('#ThinningType2').html(obj.ThinningType2);
					$('#DamageFactor').html(obj.DamageFactor);
					$('#MajorCorrosion').html(obj.MajorCorrosion);
					
					return false;
				}
		});
		return false;
	}
	
	function backButton(){
		$('#json-result').hide();
		$('#first-col').hide();
		$('#first-col').show('fast');
		$('#result').show('fast');
	}